# Face Deidentification Docker

## Docker run command

### prerequisite
- torch==1.6.0
- torchvision==0.7.0
- opencv==4.5.1.48
- CUDA 10.1

### Docker run command

```shell script
docker run -it --rm --ipc=host --gpus='"device=0"' --cpuset-cpus="0-7,16-23" -v /from/data/path:/app/data -v /to/data/path:/app/result --name face_hksong brain/face_deidentify:v1.0.0-cu101
```
```shell script
docker run -it --rm --ipc=host --gpus='"device=1"' --cpuset-cpus="8-15,24-31" -v /from/data/path:/app/data -v /to/data/path:/app/result --name face_hksong brain/face_deidentify:v1.0.0-cu101
```

### Tips for checking cpu affinity
```shell script
nvidia-smi topo --matrix
```

## After Docker run...

### File list generator

Generate video file list (finding in recursive manner) and save those in log text.
```shell script
bash file_list_generator.sh
```
or `python file_list_generator.py --log_path log.txt --input_dir data`

### Extract Videos

Extract videos whose faces are shaded off
```shell script
bash test_custom.sh
```
or `python test_custom.py --input face_deidentify_sample --vis_thres 0.3 --save_image`