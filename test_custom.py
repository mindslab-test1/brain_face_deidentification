from __future__ import print_function
from config import cfg_mnet, cfg_re50
from layers.functions.prior_box import PriorBox
from utils.nms.py_cpu_nms import py_cpu_nms
from models.retinaface import RetinaFace
from utils.box_utils import decode, decode_landm
from utils.timer import Timer

import os
import argparse
import threading

import torch
import numpy as np
import cv2
from tqdm import tqdm

from multiprocessing import Pool
from itertools import cycle

file_lock = threading.Lock()


def _check_keys(model, pretrained_state_dict):
    ckpt_keys = set(pretrained_state_dict.keys())
    model_keys = set(model.state_dict().keys())
    used_pretrained_keys = model_keys & ckpt_keys
    unused_pretrained_keys = ckpt_keys - model_keys
    missing_keys = model_keys - ckpt_keys
    # print('Missing keys:{}'.format(len(missing_keys)))
    # print('Unused checkpoint keys:{}'.format(len(unused_pretrained_keys)))
    # print('Used keys:{}'.format(len(used_pretrained_keys)))
    assert len(used_pretrained_keys) > 0, 'load NONE from pretrained checkpoint'
    return True


def _remove_prefix(state_dict, prefix):
    ''' Old style model is stored with all names of parameters sharing common prefix 'module.' '''
    # print('remove prefix \'{}\''.format(prefix))
    f = lambda x: x.split(prefix, 1)[-1] if x.startswith(prefix) else x
    return {f(key): value for key, value in state_dict.items()}


def load_model(model, pretrained_path, load_to_cpu):
    print('Loading pretrained model from {}'.format(pretrained_path))
    if load_to_cpu:
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage)
    else:
        device = torch.cuda.current_device()
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage.cuda(device))
    if "state_dict" in pretrained_dict.keys():
        pretrained_dict = _remove_prefix(pretrained_dict['state_dict'], 'module.')
    else:
        pretrained_dict = _remove_prefix(pretrained_dict, 'module.')
    _check_keys(model, pretrained_dict)
    model.load_state_dict(pretrained_dict, strict=False)
    return model


class RetinaFaceServicer():
    def __init__(self, use_cpu=False, confidence_threshold=0.02, nms_threshold=0.4,
                 top_k=7000, keep_top_k=750, vis_thres=0.5, mask_type="eye"):
        torch.set_grad_enabled(False)
        cfg = None
        if args.network == "mobile0.25":
            cfg = cfg_mnet
        elif args.network == "resnet50":
            cfg = cfg_re50

        self.cfg = cfg
        # net and model
        net = RetinaFace(cfg=cfg, phase='test')
        net = load_model(net, args.trained_model, args.cpu)
        net.eval()
        self.device = torch.device("cpu" if use_cpu else "cuda")
        self.net = net.to(self.device)
        self.resize = 0.25
        self.confidence_threshold = confidence_threshold
        self.nms_threshold = nms_threshold
        self.top_k = top_k
        self.keep_top_k = keep_top_k
        self.vis_thres = vis_thres
        if mask_type is None:
            self.mask_type = "eye"
        else:
            self.mask_type = mask_type

        self.timer = {'forward_pass': Timer(), 'misc': Timer(), 'total': Timer()}

    @staticmethod
    def _mkdir_for_result_path(result_root_dir, from_path):
        dir_map = os.path.dirname(from_path).split("/")[1:]  # ignore root

        for _depth in range(1, len(dir_map) + 1):
            file_dir = os.path.join(result_root_dir, *dir_map[:_depth])
            os.makedirs(file_dir, exist_ok=True)

        return os.path.join(result_root_dir, *dir_map, os.path.basename(from_path))

    def inference_with_videofile(self, video_path, result_root_dir):
        """
        For mps_scale, it should be used whether to do with multi-processing (or not)
        :param mps_scale:
        :return:
        """
        try:
            if not isinstance(video_path, str) or not os.path.isfile(video_path):
                raise RuntimeError("Input should be video path")

            framerate = 30000 / 1001
            cam = cv2.VideoCapture(video_path)

            # Initialize output video
            output_path = self._mkdir_for_result_path(result_root_dir, video_path)
            # fourcc = cv2.VideoWriter_fourcc(*'XVID')
            fourcc = cv2.VideoWriter_fourcc(*'mp4v')
            out = cv2.VideoWriter(output_path, fourcc, cam.get(cv2.CAP_PROP_FPS), (int(cam.get(3)), int(cam.get(4))))

            cnt = 0

            temp_boxes = None
            temp_images = []
            while True:
                _, img_raw = cam.read()
                if img_raw is None:
                    if cnt == 0:
                        raise RuntimeError("File must be media file (.mp4 supported)")
                    else:
                        break
                else:
                    box = self._inference_with_array(img_raw)
                    if len(temp_images) == 5:  # it was no human section
                        for archived_image in temp_images:
                            out.write(archived_image)
                        # reset temp element
                        temp_boxes = None
                        temp_images = []

                    if len(box) != 0:
                        if len(temp_images) != 0:  # interpolate boxes

                            interp_box = self._interpolate_box(temp_boxes, box, num_interpolate=len(temp_images))
                            # save image
                            for box_idx, archived_image in enumerate(temp_images):
                                new_masked_image = self._mask_image(archived_image, interp_box[box_idx],
                                                                    mask_type=self.mask_type)
                                out.write(new_masked_image)

                            # set new section
                            temp_boxes = box
                            temp_images = []
                            img_masked = self._mask_image(img_raw, box)
                            out.write(img_masked)


                        else:  # start new section, just save frame
                            temp_boxes = box
                            img_masked = self._mask_image(img_raw, box)
                            out.write(img_masked)

                    else:
                        if temp_boxes is not None:
                            temp_images.append(img_raw)
                        else:  # no human section
                            out.write(img_raw)

                    cnt += 1

            cam.release()
            out.release()
            return [video_path, "O", output_path]
        except RuntimeError as e:
            raise e

    #
    #
    # def inference_with_filelist(self, image_dir, mps_scale=1, result_dir="result"):
    #     """
    #     For mps_scale, it should be used whether to do with multi-processing (or not)
    #     :param mps_scale:
    #     :return:
    #     """
    #     if not isinstance(image_dir, str) or not os.path.isdir(image_dir):
    #         raise AssertionError("Input should be dirpath")
    #     else:
    #         image_list = sorted([os.path.join(image_dir, x) for x in os.listdir(image_dir)
    #                       if x.endswith(".jpg") or x.endswith(".png")])
    #
    #     num_images = len(image_list)
    #
    #     result_relative_dir = os.path.join(os.path.dirname(image_list[0]), result_dir)
    #     os.makedirs(result_relative_dir, exist_ok=True)
    #
    #     temp_boxes = None
    #     temp_images = []
    #     for i, img_name in tqdm(enumerate(image_list)):
    #         image_path = img_name
    #         img_raw = cv2.imread(image_path, cv2.IMREAD_COLOR)
    #         box = self._inference_with_array(img_raw)
    #
    #         result_filepath = os.path.join(result_relative_dir, os.path.basename(image_path))
    #         if len(temp_images) == 5:  # it was no human section
    #             for archived_image in temp_images:
    #                 cv2.imwrite(archived_image[0], archived_image[1])
    #             # reset temp element
    #             temp_boxes = None
    #             temp_images = []
    #
    #         if len(box) != 0:
    #             if len(temp_images) != 0:  # interpolate boxes
    #
    #                 interp_box = self._interpolate_box(temp_boxes, box, num_interpolate=len(temp_images))
    #                 # save image
    #                 for box_idx, archived_image in enumerate(temp_images):
    #                     new_masked_image = self._mask_image(archived_image[1], interp_box[box_idx])
    #                     cv2.imwrite(archived_image[0], new_masked_image)
    #
    #                 # set new section
    #                 temp_boxes = box
    #                 temp_images = []
    #                 img_masked = self._mask_image(img_raw, box)
    #                 cv2.imwrite(result_filepath, img_masked)
    #
    #
    #             else:  # start new section, just save frame
    #                 temp_boxes = box
    #                 img_masked = self._mask_image(img_raw, box)
    #                 cv2.imwrite(result_filepath, img_masked)
    #
    #         else:
    #             if temp_boxes is not None:
    #                 temp_images.append([result_filepath, img_raw])
    #             else:  # no human section
    #                 pass

    def _inference_with_array(self, img_raw):
        img = np.float32(img_raw)
        # img_mean_brightness = np.mean(img)
        # if img_mean_brightness < 90:
        #     add_bright = 90 - img_mean_brightness
        #     img = cv2.add(img, (add_bright, add_bright, add_bright, 0))

        if self.resize != 1:
            img = cv2.resize(img, None, None, fx=self.resize, fy=self.resize, interpolation=cv2.INTER_LINEAR)
        im_height, im_width, _ = img.shape
        scale = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
        img -= (104, 117, 123)
        img = img.transpose(2, 0, 1)
        img = torch.from_numpy(img).unsqueeze(0)
        img = img.to(self.device)
        scale = scale.to(self.device)

        self.timer['forward_pass'].tic()
        loc, conf, landms = self.net(img)  # forward pass
        self.timer['forward_pass'].toc()
        self.timer['misc'].tic()
        priorbox = PriorBox(self.cfg, image_size=(im_height, im_width))
        priors = priorbox.forward()
        priors = priors.to(self.device)
        prior_data = priors.data
        boxes = decode(loc.data.squeeze(0), prior_data, self.cfg['variance'])
        boxes = boxes * scale / self.resize
        boxes = boxes.cpu().numpy()
        scores = conf.squeeze(0).data.cpu().numpy()[:, 1]
        landms = decode_landm(landms.data.squeeze(0), prior_data, self.cfg['variance'])
        scale1 = torch.Tensor([img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2]])
        scale1 = scale1.to(self.device)
        landms = landms * scale1 / self.resize
        landms = landms.cpu().numpy()

        # ignore low scores
        inds = np.where(scores > args.confidence_threshold)[0]
        boxes = boxes[inds]
        landms = landms[inds]
        scores = scores[inds]

        # keep top-K before NMS
        # order = scores.argsort()[::-1][:args.top_k]
        order = scores.argsort()[::-1]
        boxes = boxes[order]
        landms = landms[order]
        scores = scores[order]

        # do NMS
        dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
        keep = py_cpu_nms(dets, args.nms_threshold)

        dets = dets[keep, :]
        landms = landms[keep]

        # keep top-K faster NMS
        # dets = dets[:args.keep_top_k, :]
        # landms = landms[:args.keep_top_k, :]

        dets = np.concatenate((dets, landms), axis=1)
        self.timer['misc'].toc()

        # save dets
        # fw.write('{:s}\n'.format(img_name))
        # fw.write('{:.1f}\n'.format(dets.shape[0]))
        # for k in range(dets.shape[0]):
        #     xmin = dets[k, 0]
        #     ymin = dets[k, 1]
        #     xmax = dets[k, 2]
        #     ymax = dets[k, 3]
        #     score = dets[k, 4]
        #     w = xmax - xmin + 1
        #     h = ymax - ymin + 1
        #     # fw.write('{:.3f} {:.3f} {:.3f} {:.3f} {:.10f}\n'.format(xmin, ymin, w, h, score))
        #     # fw.write('{:d} {:d} {:d} {:d} {:.10f}\n'.format(int(xmin), int(ymin), int(w), int(h), score))

        boxes = [x for x in dets if x[4] >= args.vis_thres]

        return boxes

    @staticmethod
    def _mask_image(img_raw, boxes, add_margin=True, mask_type="eye"):
        im_height, im_width, _ = img_raw.shape
        img_blur = np.copy(img_raw)
        mask = np.zeros((im_height, im_width, 1), dtype=np.uint8)
        img_result = np.copy(img_raw)
        for b in boxes:
            b = list(map(int, b))
            if add_margin:
                b[0] = max(b[0] - 20, 0)
                b[1] = max(b[1] - 20, 0)
                b[2] = min(b[2] + 20, im_width)
                b[3] = min(b[3] + 20, im_height)

            if mask_type == "eye":
                left_eye = (b[5], b[6])
                right_eye = (b[7], b[8])
                length = np.sqrt(np.power(b[7] - b[5], 2) + np.power(b[8] - b[6], 2))
                margin_length = int(length // 3)

                try:
                    angle = np.arctan((b[8] - b[6]) / (b[7] - b[5]))
                except ZeroDivisionError:
                    angle = np.arctan((b[8] - b[6]) / (b[7] - b[5]) + 0.01)

                left_top = [left_eye[0] + margin_length * np.cos(angle + np.pi * 0.75),
                            left_eye[1] + margin_length * np.sin(angle + np.pi * 0.75)]

                left_bot = [left_eye[0] + margin_length * np.cos(angle + np.pi * 1.25),
                            left_eye[1] + margin_length * np.sin(angle + np.pi * 1.25)]

                right_top = [right_eye[0] + margin_length * np.cos(angle + np.pi * 0.25),
                             right_eye[1] + margin_length * np.sin(angle + np.pi * 0.25)]

                right_bot = [right_eye[0] + margin_length * np.cos(angle - np.pi * 0.25),
                             right_eye[1] + margin_length * np.sin(angle - np.pi * 0.25)]

                mask = cv2.fillPoly(mask, np.array([[left_top, right_top, right_bot, left_bot]], dtype=np.int32), 255)
                mask_bool = (mask[..., 0] == 255)

                min_x = int(min(left_top[0], left_bot[0], right_top[0], right_bot[0]))
                max_x = int(max(left_top[0], left_bot[0], right_top[0], right_bot[0]))
                min_y = int(min(left_top[1], left_bot[1], right_top[1], right_bot[1]))
                max_y = int(max(left_top[1], left_bot[1], right_top[1], right_bot[1]))

                k_size = ((b[3] - b[1]) // 3) // 2 * 2 + 1  # If it is even, add one to make it as odd.
                try:
                    img_blur[min_y:max_y, min_x:max_x, :] = cv2.GaussianBlur(img_blur[min_y:max_y, min_x:max_x, :],
                                                                            (k_size, k_size), 0)
                except cv2.error:
                    img_blur[min_y:max_y, min_x:max_x, :] = 0

                img_result[mask_bool, :] = img_blur[mask_bool, :]


            elif mask_type == "face":
                k_size = ((b[3] - b[1]) // 3) // 2 * 2 + 1  # If it is even, add one to make it as odd.
                try:
                    img_result[b[1]:b[3], b[0]:b[2], :] = cv2.GaussianBlur(img_result[b[1]:b[3], b[0]:b[2], :],
                                                                        (k_size, k_size), 0)
                except cv2.error:
                    img_result[b[1]:b[3], b[0]:b[2], :] = 0
            else:
                raise RuntimeError("Mask type is either eye or face.")
        return img_result

    @staticmethod
    def _interpolate_box(start_box, end_box, num_interpolate):
        box_num = min(len(start_box), len(end_box))
        start_box_clipped = start_box[:box_num]
        end_box_clipped = end_box[:box_num]

        interpolated_boxes = []
        for interp_idx in range(1, num_interpolate + 1):
            box_interp = []
            start_ratio = (interp_idx / num_interpolate + 1)
            for box_idx in range(box_num):
                box_interp.append(start_box_clipped[box_idx] * start_ratio +
                                  end_box_clipped[box_idx] * (1 - start_ratio))
            interpolated_boxes.append(box_interp)

        return interpolated_boxes


def get_masked_video(params):
    retina_face_model, video_input, result_root_dir, device_id = params

    try:
        chunk_data = retina_face_model.inference_with_videofile(video_path=video_input, result_root_dir=result_root_dir)
        return chunk_data
    except RuntimeError as e:
        error_msg = str(e).replace("\n", "\t")
        print("failed for {:s}...{:s}".format(video_input, error_msg))
        return [video_input, "X", error_msg]


def scheduler(use_cpu, confidence_threshold, nms_threshold, top_k, keep_top_k, input_list, result_root_dir,
              fn, device_id="0", workers=8):
    device_ids = device_id.split(",")
    pool = Pool(processes=workers)
    for chunks_data in tqdm(pool.imap_unordered(fn, zip(cycle([use_cpu]),
                                                        cycle([confidence_threshold]),
                                                        cycle([nms_threshold]),
                                                        cycle([top_k]),
                                                        cycle([keep_top_k]),
                                                        input_list,
                                                        cycle([result_root_dir]),
                                                        cycle(device_ids)))):
        pass


def scheduler_with_filelist(use_cpu, confidence_threshold, nms_threshold, top_k, keep_top_k, filelist_path,
                            result_root_dir, mask_type, fn, device_id="0", workers=8):
    os.makedirs(result_root_dir, exist_ok=True)
    result_log_path = "{:s}_result.txt".format(os.path.splitext(filelist_path)[0])
    with open(filelist_path, 'r') as f:
        lines = [line.split(",") for line in f.readlines()]

    input_list = [row[0] for row in lines]
    print("Total Files: {:d}".format(len(input_list)))

    if os.path.exists(result_log_path):  # filter files which have been completed
        with open(result_log_path, 'r') as f:
            lines = [line.split(",") for line in f.readlines()]

        completed_files = [row[0] for row in lines if row[1] == "O"]

        # clean-up result log file
        filtered_lines = [",".join(row) for row in lines if row[1] != "X"]
        with open(result_log_path, 'w') as f:
            f.writelines(filtered_lines)

        for completed_file in completed_files:
            try:
                input_list.remove(completed_file)
            except ValueError:
                pass

    else:  # create new file
        with open(result_log_path, 'w') as f:
            f.write("file_name,succeed,result_path\n")

    print("Files To-Do: {:d}".format(len(input_list)))

    device_ids = device_id.split(",")
    pool = Pool(processes=workers)

    retina_face_models = [RetinaFaceServicer(use_cpu=use_cpu,
                                             confidence_threshold=confidence_threshold,
                                             nms_threshold=nms_threshold,
                                             top_k=top_k,
                                             keep_top_k=keep_top_k,
                                             mask_type=mask_type)
                          for _ in range(workers)]
    for chunks_data in tqdm(pool.imap_unordered(fn, zip(cycle(retina_face_models),
                                                        input_list,
                                                        cycle([result_root_dir]),
                                                        cycle(device_ids))), total=len(input_list)):
        with file_lock:
            with open(result_log_path, 'a') as f:
                f.write("{:s},{:s},{:s}\n".format(*chunks_data[:3]))


def get_argument():
    parser = argparse.ArgumentParser(description='Retinaface')

    parser.add_argument('-m', '--trained_model', default='./weights/mobilenet0.25_Final.pth',
                        type=str, help='Trained state_dict file path to open')
    parser.add_argument('-i', '--input', required=True,
                        type=str, help='Input directory or .mp4 files for processing files')
    parser.add_argument('-r', '--result_root_dir', default='./result', type=str,
                        help='where to save result videos whose hierarchy is same with the original')
    parser.add_argument('--workers', default=1, type=int,
                        help='The number of workers for multi-processing')
    parser.add_argument('--network', default='mobile0.25', help='Backbone network mobile0.25 or resnet50')
    parser.add_argument('--save_folder', default='eval/', type=str, help='Not used (hksong)')
    parser.add_argument('--mask_type', default='eye', choices=['eye', 'face'], type=str, help='Not used (hksong)')
    parser.add_argument('--cpu', action="store_true", default=False, help='Use cpu inference')
    parser.add_argument('--dataset', default='FDDB', type=str, choices=['FDDB'], help='Not used (hksong)')
    parser.add_argument('--confidence_threshold', default=0.02, type=float, help='confidence_threshold')
    parser.add_argument('--top_k', default=5000, type=int, help='top_k')
    parser.add_argument('--nms_threshold', default=0.4, type=float, help='nms_threshold')
    parser.add_argument('--keep_top_k', default=750, type=int, help='keep_top_k')
    parser.add_argument('-s', '--save_image', action="store_true", default=False, help='show detection results')
    parser.add_argument('--vis_thres', default=0.5, type=float, help='visualization_threshold')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = get_argument()
    # retina_face_model = RetinaFaceServicer(use_cpu=args.cpu,
    #                                        confidence_threshold=args.confidence_threshold,
    #                                        nms_threshold=args.nms_threshold,
    #                                        top_k=args.top_k,
    #                                        keep_top_k=args.keep_top_k)
    # retina_face_model.inference_with_videofile(video_path=args.input, postfix="_masked")

    if args.input.endswith(".txt"):  # call by text files
        scheduler_with_filelist(args.cpu, args.confidence_threshold, args.nms_threshold, args.top_k, args.keep_top_k,
                                args.input, args.result_root_dir, args.mask_type, get_masked_video,
                                device_id="0", workers=args.workers)

    else:
        video_list = sorted([os.path.join(args.input, x) for x in os.listdir(args.input)
                             if x.endswith(".mp4") and not "_masked" in x])
        scheduler(args.cpu, args.confidence_threshold, args.nms_threshold, args.top_k, args.keep_top_k,
                  video_list, args.result_root_dir, get_masked_video, device_id="0", workers=args.workers)
