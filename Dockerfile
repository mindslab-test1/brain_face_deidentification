FROM docker.maum.ai:443/brain/vision:cu101-torch160

COPY . /app
#COPY ./requirements.txt /app/
WORKDIR /app

RUN pip install -r requirements.txt
RUN pip install torchvision==0.7.0