import os
import argparse

def _check_logfile_exist(log_path):
    return os.path.exists(log_path)


def get_file_list(root_dir):
    file_list = []
    for root, sub_dirs, files in os.walk(root_dir):
        for fname in files:
            ext = os.path.splitext(fname)[-1].lower()
            if ext == ".mp4":
                file_list.append(os.path.join(root, fname))

    return file_list


def _chunks(full_list, n):
    for i in range(0, len(full_list), n):
        yield full_list[i:i + n]


def write_logfile(file_list, log_path, ignore_preexist=False):
    prefix = os.path.splitext(log_path)[0]
    if _check_logfile_exist(log_path):
        print("File Exist!!!! at {:s}".format(log_path))
        if not ignore_preexist:
            return
        else:
            print("Overwrite on {:s}".format(log_path))

    for list_idx, chunk_list in enumerate(_chunks(file_list, len(file_list) // 2 + 1)):
        with open("{:s}_{:01d}.txt".format(prefix, list_idx), "w") as f:
            for filename in chunk_list:
                f.write("{:s},NOT_YET,BLANK\n".format(filename))

    return log_path

def get_argument():
    parser = argparse.ArgumentParser(description='Retinaface File List Generator')

    parser.add_argument('-l', '--log_path', default='log.txt',
                        type=str, help='video file list')
    parser.add_argument('-i', '--input_dir', default='data',
                        type=str, help='root data directory')
    parser.add_argument('--ignore_file', action="store_true", default=False, help='whether to overwrite log files')
    args = parser.parse_args()

    return args


if __name__ == '__main__':
    args = get_argument()
    file_list = get_file_list(args.input_dir)
    _ = write_logfile(file_list, args.log_path, args.ignore_file)
